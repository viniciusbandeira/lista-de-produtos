import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { ProductOutput } from '../models/productOutput';
import { ProductInput } from '../models/productInput';
import { FilteredPage } from '../models/filteredPage';

@Injectable({
  providedIn: 'root'
})

export class ProductsService {

  constructor(private http: HttpClient) { }
    
  Get(id: number){
    return this.http
      .get<ProductOutput>(environment.apiUrl + 'Products/'+id);
  }

  Update(id: number, product: ProductInput){
    return this.http
      .put(environment.apiUrl + 'Products/'+id, product);
  }

  GetAll(){
    return this.http
      .get<ProductOutput[]>(environment.apiUrl + 'Products');
  }

  Delete(id: number){
    return this.http
      .delete(environment.apiUrl + 'Products/'+id);
  }

  Create(product: ProductInput){
    return this.http
      .post(environment.apiUrl + 'Products/', product);
  }

  GetFilteredPage(pageIndex: number, pageSize: number, filter?: string){
    var params = new HttpParams()
      .set('pageIndex', pageIndex)
      .set('pageSize', pageSize);

      if(filter){
        params = params.set('filter', filter);
      }
    return this.http
      .get<FilteredPage>(environment.apiUrl+'Products/GetFilteredPage', {params});
  }
}
