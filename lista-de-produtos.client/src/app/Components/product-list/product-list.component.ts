import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import { MatTable } from '@angular/material/table';
import { MatPaginator, PageEvent} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ProductsService } from '../../services/products.service';

import { ToastrService } from 'ngx-toastr';
import { ProductOutput } from '../../models/productOutput';
import { Router } from '@angular/router';
import { Subject, debounceTime, distinctUntilChanged } from 'rxjs';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrl: './product-list.component.css'
})
export class ProductListComponent implements OnInit {
  @ViewChild(MatTable) table!: MatTable<ProductOutput>;

  public products: ProductOutput[] = [];

  constructor(
    private productsService: ProductsService,
    private toastr: ToastrService,
    private router: Router){

      this.filterUpdate.pipe(
        debounceTime(400),
        distinctUntilChanged())
        .subscribe(value => {
          this.GetProducts(0,this.pageSize, this.filter);
        });

  }

  filterUpdate = new Subject<string>()
  pageIndex = 0;
  pageSize = 10;
  pageSizeOptions = [5, 10];
  length = 500;
  filter = '';

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'name', 'description', 'price', 'createdDate', 'action'];


  handlePageEvent(event: PageEvent) {
    this.length = event.length;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    this.GetProducts(this.pageIndex, this.pageSize);
  }

  ngOnInit(){
    this.GetProducts(this.pageIndex, this.pageSize, this.filter);
  }



  GetProducts(pageIndex: number, pageSize: number, filter?: string){
    this.productsService.GetFilteredPage(pageIndex + 1, pageSize, filter).subscribe(data => {
      console.log(data);
      this.products = data.itens;
      this.table.dataSource = this.products;
      this.pageIndex = data.pageIndex -1;
      this.length = data.totalCount;
      this.pageSize = pageSize;
    });
    // this.productsService.GetAll().subscribe(data => {
    //   this.products = data
    //   this.table.dataSource = this.products
    // });
  }

  delete(id:number){
    this.productsService.Delete(id).subscribe(() => {
      this.GetProducts(this.pageIndex, this.pageSize);
      this.toastr.success('Produto deletado!');
    });
  }

  edit(id:number){
    this.router.navigateByUrl('/product-edit/' + id);
  }
}
