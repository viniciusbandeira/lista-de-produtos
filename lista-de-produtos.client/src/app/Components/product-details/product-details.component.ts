import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductsService } from '../../services/products.service';
import { ProductOutput } from '../../models/productOutput';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrl: './product-details.component.css'
})
export class ProductDetailsComponent implements OnInit{

  constructor(
    private route: ActivatedRoute,
    private productsService: ProductsService,){}

  productId = 0;
  product: ProductOutput | undefined;

  ngOnInit(){
    this.productId = this.route.snapshot.params['id'];
    if (this.productId) {
      this.productsService.Get(this.productId).subscribe(data => {
        this.product = data;
        console.log(this.product);
      });
    }
  }
}
