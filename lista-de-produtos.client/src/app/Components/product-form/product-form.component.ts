import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ProductsService } from '../../services/products.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductInput } from '../../models/productInput';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrl: './product-form.component.css'
})
export class ProductFormComponent implements OnInit{

  constructor(
    private formBuilder: FormBuilder,
    private productsService: ProductsService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router){}

  productForm = this.formBuilder.group({
    name: ['', Validators.required],
    description: ['', Validators.required],
    price: [0, Validators.required]
  });
  productId!: number;
  isEdit = false;


  ngOnInit(){
    this.productId = this.route.snapshot.params['id'];
    if (this.productId) {
      this.isEdit = true;
      this.productsService.Get(this.productId).subscribe(data => {
        this.productForm.patchValue(data);
      });
    }
  }

  Save(){

    var product: ProductInput = {
      name: this.productForm.value.name!,
      description: this.productForm.value.description!,
      price: this.productForm.value.price!
    }
    
    if(this.isEdit){
      this.productsService.Update( this.productId, product).subscribe(() => {
        this.toastr.success("Produto Editado!");
        this.router.navigateByUrl("/");
      })
    }else{
      this.productsService.Create(product).subscribe(() => {
        this.toastr.success("Produto Criado!");
        this.router.navigateByUrl("/");
      })
    }
  }

}
