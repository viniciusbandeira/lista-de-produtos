export interface ProductOutput{
    id: number;
    name: string;
    description: string;
    price: number;
    createdDate: string; 
}