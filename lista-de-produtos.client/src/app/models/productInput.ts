export interface ProductInput{
    name: string;
    description: string;
    price: number;
}
