import { ProductOutput } from "./productOutput";

export interface FilteredPage{
    itens: ProductOutput[],
    pageSize: number,
    pageIndex: number,
    totalCount: number,
    filter: string
  }