import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductListComponent } from './Components/product-list/product-list.component';
import { ProductFormComponent } from './Components/product-form/product-form.component';
import { ProductDetailsComponent } from './Components/product-details/product-details.component';

const routes: Routes = [
  {path: '', component: ProductListComponent },
  {path: 'product-list', component: ProductListComponent },
  {path: 'product-create', component: ProductFormComponent },
  {path: 'product-edit/:id', component: ProductFormComponent },
  {path: 'product-details/:id', component: ProductDetailsComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
