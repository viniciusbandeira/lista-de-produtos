# Product List

Product-list is a CRUD solution with Angular and .NET 8.0 using SQL Server

## Documentation

- [RoadMap](https://trello.com/b/SlzLcnal/roadmap-projeto-desafio-lista-produtos)
- [Requirements](https://gitlab.com/viniciusbandeira/lista-de-produtos/-/wikis/Requirements)

## Installation

### Frontend

Prerequisites: 
- [Node](https://nodejs.org/en/download) ^20.9.0

Inside the `.\lista-de-produtos.client\` Folder execute this Commands:

```bash
npm install
npm run start
```

After this access `http://localhost:4200/`

### Backend
The Backend Development environment use docker and autofill the database automatically

Prerequisites: 
- [.NET 8.0 SDK](https://dotnet.microsoft.com/pt-br/download/visual-studio-sdks) ^8.0.200
- [Visual Studio](https://visualstudio.microsoft.com/pt-br/downloads/) ^17.8
- [Docker](https://www.docker.com/products/docker-desktop/) ^4.28.0

Open `lista-de-produtos.sln` and use `docker-compose` as startup project and run `Docker Compose` launch setting


After this the swagger will open in `https://localhost:8080/swagger/index.html`

## Technologies
- Angular 17
- ASP.NET 8.0
- ASP.NET WebApi
- Entity Framework Core Code First
- xUnit
- Moq
- Fluent Assertion
- TailWind CSS
- Angular Material
- Swagger UI
- AutoMapper
- Docker
- DDD


## License

[MIT](https://choosealicense.com/licenses/mit/)