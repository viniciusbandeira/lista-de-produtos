﻿using lista_de_produtos.Server.Domain;
using lista_de_produtos.Server.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lista_de_produtos.Server.Test.Util
{
    public static class ProductUtil
    {
        public static List<Product> GetMockProducts()
        {
            var list = new List<Product>();
            list.Add(new Product
            {
                CreatedDate = DateTime.Parse("2024-04-07T16:54:20.6092282-03:00"),
                Name = "Valid Name",
                Price = 12345,
                Description = "Description",
                Id = 1
            });

            list.Add(new Product
            {
                CreatedDate = DateTime.Parse("2024-04-07T16:54:20.6092282-03:00"),
                Name = "Valid Name",
                Price = 12345,
                Description = "Description",
                Id = 2
            });

            list.Add(new Product
            {
                CreatedDate = DateTime.Parse("2024-04-07T16:54:20.6092282-03:00"),
                Name = "Valid Name",
                Price = 12345,
                Description = "Description",
                Id = 3
            });

            return list;
        }

        public static List<ProductOutputViewModel> GetMockProductsOutputViewModel()
        {
            var list = new List<ProductOutputViewModel>();
            list.Add(new ProductOutputViewModel
            {
                CreatedDate = DateTime.Parse("2024-04-07T16:54:20.6092282-03:00"),
                Name = "Valid Name",
                Price = 12345,
                Description = "Description",
                Id = 1
            });

            list.Add(new ProductOutputViewModel
            {
                CreatedDate = DateTime.Parse("2024-04-07T16:54:20.6092282-03:00"),
                Name = "Valid Name",
                Price = 12345,
                Description = "Description",
                Id = 2
            });

            list.Add(new ProductOutputViewModel
            {
                CreatedDate = DateTime.Parse("2024-04-07T16:54:20.6092282-03:00"),
                Name = "Valid Name",
                Price = 12345,
                Description = "Description",
                Id = 3
            });

            return list;
        }
        
        public static ProductOutputViewModel GetMockProductOutputViewModel()
        {
            return new ProductOutputViewModel
            {
                CreatedDate = DateTime.Parse("2024-04-07T16:54:20.6092282-03:00"),
                Name = "Valid Name",
                Price = 12345,
                Description = "Description",
                Id = 1
            };
        }

        public static Product GetProduct()
        {
            return new Product
            {
                CreatedDate = DateTime.Parse("2024-04-07T16:54:20.6092282-03:00"),
                Name = "Valid Name",
                Price = 12345,
                Description = "Description",
                Id = 1
            };
        }

        public static ProductInputViewModel GetProductInputViewModel()
        {
            return new ProductInputViewModel
            {
                Name = "Valid Name",
                Price = 12345,
                Description = "Description"
            };
        }

        public static ProductInputViewModel GetInvalidProductInputViewModel()
        {
            return new ProductInputViewModel
            {
                Name = "Name",
                Price = 12345,
                Description = "D"
            };
        }
    }


}
