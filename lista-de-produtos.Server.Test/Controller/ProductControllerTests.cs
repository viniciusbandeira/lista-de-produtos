﻿using FluentAssertions;
using lista_de_produtos.Server.Domain.Interfaces;
using lista_de_produtos.Server.Domain.ViewModels;
using lista_de_produtos.Server.Test.Util;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lista_de_produtos.Server.Test.Controller
{
    public class ProductControllerTests
    {
        private ProductsController _productsController;
        private Mock<IProductService> _productServiceMock;
        public ProductControllerTests() 
        {
            Mock<IProductService> mockService = new Mock<IProductService>();
            _productServiceMock = mockService;
            ProductsController controller = new ProductsController(mockService.Object);
            _productsController = controller;
        }



        [Fact]
        public async Task GetAllProducts200()
        {
            _productServiceMock.Setup(x => x.GetAllProducts()).ReturnsAsync(ProductUtil.GetMockProductsOutputViewModel());


            var result = await _productsController.Get() as OkObjectResult;

            result?.StatusCode.Should().Be(200);
            _productServiceMock.Verify(x => x.GetAllProducts(), Times.Once);
        }

        [Fact]
        public async Task GetAllProducts400()
        {
            _productServiceMock.Setup(x => x.GetAllProducts()).ThrowsAsync(new Exception());


            var result = await _productsController.Get() as OkObjectResult;

            result?.StatusCode.Should().Be(400);
            _productServiceMock.Verify(x => x.GetAllProducts(), Times.Once);
        }

        [Fact]
        public async Task GetById200()
        {
            _productServiceMock.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(ProductUtil.GetMockProductOutputViewModel());


            var result = await _productsController.Get(1) as OkObjectResult;

            result?.StatusCode.Should().Be(200);
            _productServiceMock.Verify(x => x.GetById(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public async Task GetById404()
        {
            _productServiceMock.Setup(x => x.GetById(It.IsAny<int>())).ThrowsAsync(new KeyNotFoundException());


            var result = await _productsController.Get(1) as OkObjectResult;

            result?.StatusCode.Should().Be(404);
            _productServiceMock.Verify(x => x.GetById(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public async Task GetById400()
        {
            _productServiceMock.Setup(x => x.GetById(It.IsAny<int>())).ThrowsAsync(new Exception());


            var result = await _productsController.Get(1) as OkObjectResult;

            result?.StatusCode.Should().Be(400);
            _productServiceMock.Verify(x => x.GetById(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public async Task Create200()
        {
            _productServiceMock.Setup(x => x.Create(It.IsAny<ProductInputViewModel>())).ReturnsAsync(ProductUtil.GetMockProductOutputViewModel());


            var result = await _productsController.Create(ProductUtil.GetProductInputViewModel()) as OkObjectResult;

            result?.StatusCode.Should().Be(200);
            _productServiceMock.Verify(x => x.Create(It.IsAny<ProductInputViewModel>()), Times.Once);
        }

        [Fact]
        public async Task Create400()
        {
            _productServiceMock.Setup(x => x.Create(It.IsAny<ProductInputViewModel>())).ThrowsAsync(new Exception());


            var result = await _productsController.Create(ProductUtil.GetProductInputViewModel()) as OkObjectResult;


            result?.StatusCode.Should().Be(400);
            _productServiceMock.Verify(x => x.Create(It.IsAny<ProductInputViewModel>()), Times.Once);
        }

        [Fact]
        public async Task Delete200()
        {
            _productServiceMock.Setup(x => x.DeleteById(It.IsAny<int>())).Returns(Task.FromResult(default(object)));


            var result = await _productsController.Delete(1) as OkObjectResult;

            result?.StatusCode.Should().Be(200);
            _productServiceMock.Verify(x => x.DeleteById(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public async Task Delete400()
        {
            _productServiceMock.Setup(x => x.DeleteById(It.IsAny<int>())).ThrowsAsync(new Exception());


            var result = await _productsController.Delete(1) as OkObjectResult;



            result?.StatusCode.Should().Be(400);
            _productServiceMock.Verify(x => x.DeleteById(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public async Task Delete404()
        {
            _productServiceMock.Setup(x => x.DeleteById(It.IsAny<int>())).ThrowsAsync(new KeyNotFoundException());


            var result = await _productsController.Delete(1) as OkObjectResult;



            result?.StatusCode.Should().Be(404);
            _productServiceMock.Verify(x => x.DeleteById(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public async Task Update200()
        {
            _productServiceMock.Setup(x => x.Update(It.IsAny<int>(), It.IsAny<ProductInputViewModel>())).ReturnsAsync(ProductUtil.GetMockProductOutputViewModel());


            var result = await _productsController.UpdateProduct(1, ProductUtil.GetProductInputViewModel()) as OkObjectResult;

            result?.StatusCode.Should().Be(200);
            _productServiceMock.Verify(x => x.Update(It.IsAny<int>(), It.IsAny<ProductInputViewModel>()), Times.Once);
        }

        [Fact]
        public async Task Update400()
        {
            _productServiceMock.Setup(x => x.Update(It.IsAny<int>(), It.IsAny<ProductInputViewModel>())).ThrowsAsync(new Exception());


            var result = await _productsController.UpdateProduct(1, ProductUtil.GetProductInputViewModel()) as OkObjectResult;



            result?.StatusCode.Should().Be(400);
            _productServiceMock.Verify(x => x.Update(It.IsAny<int>(), It.IsAny<ProductInputViewModel>()), Times.Once);
        }

        [Fact]
        public async Task Update404()
        {
            _productServiceMock.Setup(x => x.Update(It.IsAny<int>(), It.IsAny<ProductInputViewModel>())).ThrowsAsync(new KeyNotFoundException());


            var result = await _productsController.UpdateProduct(1, ProductUtil.GetProductInputViewModel()) as OkObjectResult;



            result?.StatusCode.Should().Be(404);
            _productServiceMock.Verify(x => x.Update(It.IsAny<int>(), It.IsAny<ProductInputViewModel>()), Times.Once);
        }



    }
}
