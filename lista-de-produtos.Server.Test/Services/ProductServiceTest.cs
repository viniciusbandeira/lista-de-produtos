﻿using AutoMapper;
using FluentAssertions;
using lista_de_produtos.Server.Domain;
using lista_de_produtos.Server.Domain.Interfaces;
using lista_de_produtos.Server.Domain.ViewModels;
using lista_de_produtos.Server.Services;
using lista_de_produtos.Server.Services.AutoMapper;
using lista_de_produtos.Server.Test.Util;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lista_de_produtos.Server.Test.Services
{
    public class ProductServiceTest
    {
        private ProductService _productService;
        private Mock<IProductRepository> _productRepositoryMock;
        public ProductServiceTest() 
        {
            Mock<IProductRepository> mockRepo = new Mock<IProductRepository>();
            _productRepositoryMock = mockRepo;

            var mapper = new MapperConfiguration(c =>
                c.AddProfile<ProductProfile>()).CreateMapper();

            ProductService productService = new ProductService(mockRepo.Object, mapper);

            _productService = productService;


        }


        [Fact]
        public async Task GetAllProductsSuccess()
        {
            _productRepositoryMock.Setup(x => x.GetAllProducts()).ReturnsAsync(ProductUtil.GetMockProducts());

            var result = await _productService.GetAllProducts();

            result.Should().BeEquivalentTo(ProductUtil.GetMockProductsOutputViewModel());
        }

        [Fact]
        public async Task GetProductByIdSuccess()
        {
            _productRepositoryMock.Setup(x => x.GetById(1)).ReturnsAsync(ProductUtil.GetProduct());

            var result = await _productService.GetById(1);

            result.Should().BeEquivalentTo(ProductUtil.GetMockProductOutputViewModel());
        }

        [Fact]
        public async Task GetProductByIdFail()
        {
            _productRepositoryMock.Setup(x => x.GetById(It.IsAny<int>())).ThrowsAsync(new KeyNotFoundException("Product not found"));

            var result = async () => await _productService.GetById(1);

            await result.Should().ThrowAsync<KeyNotFoundException>();
        }

        [Fact]
        public async Task DeleteByIdSuccess()
        {
            _productRepositoryMock.Setup(x => x.DeleteById(It.IsAny<int>())).Returns(Task.FromResult(default(object)));

            await _productService.DeleteById(1);

            _productRepositoryMock.Verify(x => x.DeleteById(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public async Task DeleteByIdFail()
        {
            _productRepositoryMock.Setup(x => x.DeleteById(It.IsAny<int>())).ThrowsAsync(new KeyNotFoundException("Product not found"));

            var result = async () => await _productService.DeleteById(1);

            await result.Should().ThrowAsync<KeyNotFoundException>();
        }

        [Fact]
        public async Task CreateSuccess()
        {
            _productRepositoryMock.Setup(x => x.Create(It.IsAny<Product>())).ReturnsAsync(ProductUtil.GetProduct());

            var result = await _productService.Create(ProductUtil.GetProductInputViewModel());


            result.Should().BeEquivalentTo(ProductUtil.GetMockProductOutputViewModel());
            _productRepositoryMock.Verify(x => x.Create(It.IsAny<Product>()), Times.Once);
        }

        [Fact]
        public async Task CreateFail()
        {
            _productRepositoryMock.Setup(x => x.Create(It.IsAny<Product>())).ReturnsAsync(ProductUtil.GetProduct());

            var result = () => _productService.Create(ProductUtil.GetInvalidProductInputViewModel());


            await result.Should().ThrowAsync<ArgumentException>();
            _productRepositoryMock.Verify(x => x.Create(It.IsAny<Product>()), Times.Never);
        }

        [Fact]
        public async Task UpdateSuccess()
        {
            _productRepositoryMock.Setup(x => x.Update(It.IsAny<int>(),It.IsAny<Product>())).ReturnsAsync(ProductUtil.GetProduct());

            var result = await _productService.Update(1, ProductUtil.GetProductInputViewModel());


            result.Should().BeEquivalentTo(ProductUtil.GetMockProductOutputViewModel());
            _productRepositoryMock.Verify(x => x.Update(It.IsAny<int>(),It.IsAny<Product>()), Times.Once);
        }

        [Fact]
        public async Task UpdateFail()
        {
            _productRepositoryMock.Setup(x => x.Update(It.IsAny<int>(), It.IsAny<Product>())).ReturnsAsync(ProductUtil.GetProduct());

            var result = () => _productService.Update(1, ProductUtil.GetInvalidProductInputViewModel());


            await result.Should().ThrowAsync<ArgumentException>();
            _productRepositoryMock.Verify(x => x.Update(It.IsAny<int>(), It.IsAny<Product>()), Times.Never);
        }


    }
}
