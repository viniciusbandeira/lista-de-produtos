﻿using AutoMapper;
using lista_de_produtos.Server.Domain;
using lista_de_produtos.Server.Domain.ViewModels;

namespace lista_de_produtos.Server.Services.AutoMapper
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<Product, ProductOutputViewModel>();
            CreateMap<ProductInputViewModel, Product>();
        }
    }
}
