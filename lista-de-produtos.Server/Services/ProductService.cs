﻿using AutoMapper;
using lista_de_produtos.Server.Domain;
using lista_de_produtos.Server.Domain.Interfaces;
using lista_de_produtos.Server.Domain.ViewModels;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace lista_de_produtos.Server.Services
{
    public class ProductService : IProductService
    {
        private readonly IMapper _mapper;
        private readonly IProductRepository _repository;
        public ProductService(IProductRepository productRepository, IMapper mapper)
        {
            _repository = productRepository;
            _mapper = mapper;
        }

        public async Task<ProductOutputViewModel> Create(ProductInputViewModel product)
        {
            var productDb = _mapper.Map<Product>(product);
            productDb.CreatedDate = DateTime.Now;

            Validate(productDb); 

            return _mapper.Map<ProductOutputViewModel>(await _repository.Create(productDb));
        }

        public async Task DeleteById(int id)
        {
            try
            {
                await _repository.DeleteById(id);
            }
            catch
            {
                throw;
            }
        }

        public async Task<List<ProductOutputViewModel>> GetAllProducts()
        {
            return _mapper.Map<List<ProductOutputViewModel>>(await _repository.GetAllProducts());
        }

        public async  Task<FilteredPage> GetFilteredPage(int pageIndex, int pageSize, string filter)
        {
            return await _repository.GetFilteredPage(pageIndex, pageSize, filter);
        }

        public async Task<ProductOutputViewModel> GetById(int id)
        {
            try
            {
                return _mapper.Map<ProductOutputViewModel>(await _repository.GetById(id));
            }
            catch
            {
                throw;
            }
        }

        public async Task<ProductOutputViewModel> Update(int id, ProductInputViewModel product)
        {
            try
            {
                Product productDb = _mapper.Map<Product>(product);

                Validate(productDb);

                return _mapper.Map<ProductOutputViewModel>(await _repository.Update(id, productDb));
            }
            catch
            {
                throw;
            }
        }

        private void Validate<T>(T obj)
        {
            ICollection<ValidationResult> validationResults = null;
            if (!InitialValidate(obj, out validationResults))
            {
                StringBuilder message = new StringBuilder();
                foreach (var validationResult in validationResults)
                {
                    message.AppendLine(validationResult.ErrorMessage);
                }

                throw new ArgumentException(message.ToString());
            }

        }
        private bool InitialValidate<T>(T obj, out ICollection<ValidationResult> results)
        {
            results = new List<ValidationResult>();

            return Validator.TryValidateObject(obj, new ValidationContext(obj), results, true);
        }
    }
}
