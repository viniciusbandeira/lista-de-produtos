﻿using lista_de_produtos.Server.Domain;
using lista_de_produtos.Server.Domain.Interfaces;
using lista_de_produtos.Server.Domain.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace lista_de_produtos.Server.Data
{
    public class ProductRepository : IProductRepository
    {
        private readonly ProductListDbContext _context;
        public ProductRepository(ProductListDbContext context)
        {

            _context = context;

        }

        public async Task<Product> Create(Product product)
        {
            await _context.Products.AddAsync(product);
            await _context.SaveChangesAsync();
            return product;
        }

        public async Task<Product> GetById(int id)
        {
            var result = await _context.Products.FindAsync(id);
            if (result != null)
            {
                return result;
            }else { 
                throw new KeyNotFoundException("Product not found");
            }
        }

        public async Task<List<Product>> GetAllProducts()
        {
            return await _context.Products.ToListAsync();
        }

        public async Task<FilteredPage> GetFilteredPage(int pageIndex, int pageSize, string filter)
        {
            var query = GetProductQuery(filter);

            int totalNumber = await query.CountAsync();

            List<Product> paginetedItems = await  query
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            return new FilteredPage()
            {
                Filter = filter,
                Itens = paginetedItems,
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalCount = totalNumber
            };


        }

        public async Task DeleteById(int id)
        {
            var product = await _context.Products.FindAsync(id);
            if (product != null)
            {
                _context.Products.Remove(product);
                await _context.SaveChangesAsync();
            }
            else
            {
                throw new KeyNotFoundException("Product not found");
            }
        }

        public async Task<Product> Update(int id, Product product)
        {
            var dbProduct = await _context.Products.FindAsync(id);

            if (dbProduct != null)
            {
                dbProduct.Name = product.Name;
                dbProduct.Description = product.Description;
                dbProduct.Price = product.Price;
                await _context.SaveChangesAsync();
                return dbProduct;
            }
            else
            {
                throw new KeyNotFoundException("Product not found");
            }
        }

        private IQueryable<Product> GetProductQuery( string? filter)
        {
            IQueryable<Product> query =  _context.Products;

            if(!filter.IsNullOrEmpty())
            {
                query = query.Where(x =>
                x.Name.ToLower().Contains(filter.ToLower()) ||
                x.Description.ToLower().Contains(filter.ToLower())
                //x.Id.Equals(Convert.ToInt32(filter)) ||
                //x.Price.Equals(Convert.ToInt32(filter))
                );

            }
            return query.Select(x => new Product
            {
                Id = x.Id,
                CreatedDate = x.CreatedDate,
                Description = x.Description,
                Name = x.Name,
                Price = x.Price
            }
                ); ;

        }
    }
}
