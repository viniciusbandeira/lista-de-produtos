﻿using lista_de_produtos.Server.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;

namespace lista_de_produtos.Server.Data
{
    public class ProductListDbContext :DbContext
    {
        public ProductListDbContext(DbContextOptions<ProductListDbContext> options)
    : base(options)
        {
            var dbCreater = Database.GetService<IDatabaseCreator>() as RelationalDatabaseCreator;

            if (dbCreater != null)
            {
                // Create Database 
                if (!dbCreater.CanConnect())
                {
                    dbCreater.Create();
                }

                // Create Tables
                if (!dbCreater.HasTables())
                {
                    dbCreater.CreateTables();
                }
            }

        }

        public DbSet<Product> Products { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().HasData(
                new Product() { Id = 1, Name = "Apple iPad", Description = "Tablet", Price = 1000 , CreatedDate = DateTime.Now },
                new Product() { Id = 2, Name = "Samsung Smart TV", Description = "Televison ", Price = 1500, CreatedDate=DateTime.Now },
                new Product() { Id = 3, Name = "Samsung Smart TV", Description = "Televison ", Price = 1500, CreatedDate = DateTime.Now },
                new Product() { Id = 4, Name = "Samsung Smart TV", Description = "Televison ", Price = 1500, CreatedDate = DateTime.Now },
                new Product() { Id = 5, Name = "Samsung Smart TV", Description = "Televison ", Price = 1500, CreatedDate = DateTime.Now },
                new Product() { Id = 6, Name = "Samsung Smart TV", Description = "Televison ", Price = 1500, CreatedDate = DateTime.Now },
                new Product() { Id = 7, Name = "Samsung Smart TV", Description = "Televison ", Price = 1500, CreatedDate = DateTime.Now },
                new Product() { Id = 8, Name = "Samsung Smart TV", Description = "Televison ", Price = 1500, CreatedDate = DateTime.Now },
                new Product() { Id = 9, Name = "Samsung Smart TV", Description = "Televison ", Price = 1500, CreatedDate = DateTime.Now },
                new Product() { Id = 10, Name = "Samsung Smart TV", Description = "Televison ", Price = 1500, CreatedDate = DateTime.Now },
                new Product() { Id = 11, Name = "Samsung Smart TV", Description = "Televison ", Price = 1500, CreatedDate = DateTime.Now },
                new Product() { Id = 12, Name = "Samsung Smart TV", Description = "Televison ", Price = 1500, CreatedDate = DateTime.Now },
                new Product() { Id = 13, Name = "Nokia 130", Description = "CellPhone", Price = 1200, CreatedDate = DateTime.Now });
                
                
        }


    }
}
