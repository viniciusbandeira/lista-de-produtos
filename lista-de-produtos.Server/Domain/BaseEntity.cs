﻿using System.ComponentModel.DataAnnotations;

namespace lista_de_produtos.Server.Domain
{
    public class BaseEntity
    {
        [Key]
        public int Id { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
