﻿namespace lista_de_produtos.Server.Domain.ViewModels
{
    public class ProductOutputViewModel: ProductInputViewModel
    {
        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
