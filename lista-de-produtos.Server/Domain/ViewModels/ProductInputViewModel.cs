﻿using System.ComponentModel.DataAnnotations;

namespace lista_de_produtos.Server.Domain.ViewModels
{
    public class ProductInputViewModel
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }
    }
}
