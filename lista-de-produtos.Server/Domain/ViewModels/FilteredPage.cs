﻿namespace lista_de_produtos.Server.Domain.ViewModels
{
    public class FilteredPage
    {
        public List<Product> Itens { get; set; } = new List<Product>();
        public int PageSize { get; set; } = 15;
        public int PageIndex { get; set; } = 1;
        public int TotalCount { get; set; }
        public string Filter { get; set; } = "";
    }
}
