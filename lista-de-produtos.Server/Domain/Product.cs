﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace lista_de_produtos.Server.Domain
{

    //ID (único), Nome, Descrição, Preço e Data de Criação. 
    public class Product : BaseEntity
    {
        [MaxLength(40), MinLength(5)]
        public string Name { get; set; }

        [MaxLength(100), MinLength(5)]
        public string Description { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal Price { get; set; }

    }
}
