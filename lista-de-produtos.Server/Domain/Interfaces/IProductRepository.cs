﻿using lista_de_produtos.Server.Domain;
using lista_de_produtos.Server.Domain.ViewModels;

namespace lista_de_produtos.Server.Domain.Interfaces
{
    public interface IProductRepository
    {
        Task<Product> Create(Product product);
        Task DeleteById(int id);
        Task<List<Product>> GetAllProducts();
        Task<Product> GetById(int id);
        Task<FilteredPage> GetFilteredPage(int pageIndex, int pageSize, string filter);
        Task<Product> Update(int id, Product product);
    }
}