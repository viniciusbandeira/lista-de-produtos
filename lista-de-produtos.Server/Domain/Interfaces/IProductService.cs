﻿using lista_de_produtos.Server.Domain;
using lista_de_produtos.Server.Domain.ViewModels;

namespace lista_de_produtos.Server.Domain.Interfaces
{
    public interface IProductService
    {
        Task<ProductOutputViewModel> Create(ProductInputViewModel product);
        Task DeleteById(int id);
        Task<List<ProductOutputViewModel>> GetAllProducts();
        Task<ProductOutputViewModel> GetById(int id);
        Task<FilteredPage> GetFilteredPage(int pageIndex, int pageSize, string filter);
        Task<ProductOutputViewModel> Update(int id, ProductInputViewModel product);
    }
}