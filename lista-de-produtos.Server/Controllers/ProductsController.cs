﻿using lista_de_produtos.Server.Domain.Interfaces;
using lista_de_produtos.Server.Domain.ViewModels;
using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("[controller]")]
public class ProductsController : ControllerBase
{
    private readonly IProductService _productService;

    public ProductsController(IProductService productService)
    {
        _productService = productService;
    }


    /// <summary>
    /// Get All Products
    /// </summary>
    /// <response code="200">Returns all Products</response>
    /// <response code="400">Returns the erro message</response>
    [HttpGet]
    [ProducesResponseType(typeof(List<ProductOutputViewModel>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> Get()
    {
        try
        {
            var result =  await _productService.GetAllProducts();
            return Ok(result);
        }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    /// <summary>
    /// Get a specific Product by id.
    /// </summary>
    /// <response code="200">Returns the Product Data</response>
    /// <response code="404">Product Not Found</response>
    /// <response code="400">Returns the erro message</response>
    [HttpGet("{id}")]
    [ProducesResponseType(typeof(ProductOutputViewModel), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> Get(int id)
    {
        try
        {
            var result = await _productService.GetById(id);
            return Ok(result);
        }
        catch (KeyNotFoundException ex)
        {
            return NotFound(ex.Message);
        }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    /// <summary>
    /// Get Paginated Data
    /// </summary>
    /// <response code="200">Returns the FilteredPage Data</response>
    [HttpGet]
    [Route("GetFilteredPage")]
    [ProducesResponseType(typeof(FilteredPage), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetFilteredPage(int pageIndex, int pageSize, string filter = "")
    {
        try
        {
            var result = await _productService.GetFilteredPage(pageIndex, pageSize, filter);
            return Ok(result);

        }catch(Exception ex)
        {
            return BadRequest(ex);
        }
    }

    /// <summary>
    /// Create a Product.
    /// </summary>
    /// <response code="200">Returns the created Product</response>
    /// <response code="400">Returns the erro message</response>
    [HttpPost]
    [ProducesResponseType(typeof(ProductOutputViewModel), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> Create([FromBody] ProductInputViewModel product)
    {
        try
        {
            await _productService.Create(product);
            return Ok(product);
        }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }

    }


    /// <summary>
    /// Update a specific Product by id.
    /// </summary>
    /// <response code="200">Return the Updated Product</response>
    /// <response code="404">Product Not Found</response>
    /// <response code="400">Returns the erro message</response>
    [HttpPut("{id}")]
    [ProducesResponseType(typeof(ProductOutputViewModel), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> UpdateProduct([FromRoute] int id, [FromBody] ProductInputViewModel product)
    {
        try
        {
            var dbProduct = await _productService.Update(id, product);
            return Ok();
        }
        catch (KeyNotFoundException ex)
        {
            return NotFound(ex.Message);
        }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    /// <summary>
    /// Delete a specific Product by id.
    /// </summary>
    /// <response code="200">Product Deleted</response>
    /// <response code="404">Product Not Found</response>
    /// <response code="400">Returns the erro message</response>
    [HttpDelete("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> Delete(int id)
    {
        try
        {
            await _productService.DeleteById(id);
            return Ok();
        }
        catch (KeyNotFoundException ex)
        {
            return NotFound(ex.Message);
        }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }
}